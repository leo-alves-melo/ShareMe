#!/bin/bash

rm -rf ~/Library/Developer/Xcode/DerivedData/*
pod deintegrate
rm Podfile.lock
pod repo update
pod install

