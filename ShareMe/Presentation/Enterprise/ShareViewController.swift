//
//  ShareViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 22/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class ShareViewController: UIViewController, UITextViewDelegate {
    
    var post:Post!
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var waitActivity: UIActivityIndicatorView!
    @IBOutlet weak var informationLabel: UILabel!

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var copyTextButton: UIButton!
    var textToBeShare:String!
    var userReach = 0
    var points = 0
    
    var beginEditing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shareButton.layer.cornerRadius = 5
        self.shareButton.layer.masksToBounds = true
        
        textToBeShare = post.text! + "\n\n(Compartilhe e ganhe descontos com o aplicativo Desconta aê!)"
        postImageView.image = post.imageLinked!.image!
        
        self.waitActivity.startAnimating()
        
        self.textView.delegate = self
        self.textView.layer.borderColor = UIColor.black.cgColor
        self.textView.layer.borderWidth = 1
        
        let facebookService = FacebookService()
        facebookService.getFriendsCountDelegate = self
        facebookService.getNumberOfFriends()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
    }
    
    @objc func endEditing() {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        beginEditing = true
    }
    
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}
    
    @IBAction func cancelButtonDidPressed(_ sender: Any) {
        performSegue(withIdentifier: "unwind", sender: nil)
    }

    @IBAction func copyWithPostText(_ sender: Any) {
        UIPasteboard.general.string = textToBeShare
    }
    
    
    @IBAction func shareButtonDidClicked(_ sender: Any) {
        self.waitingView.isHidden = false
        self.waitActivity.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //Se o usuário não digitou nem clicou no botão que escreve, apaga a mensagem
        if !beginEditing {
            self.textView.text = ""
        }
        
        
        
        if textView.text == textToBeShare {
            points = userReach
        }
        else {
            points = userReach/3
        }

        let facebookService = FacebookService()
        facebookService.shareDelegate = self
        facebookService.share(text: textView.text, image: self.post.imageLinked!.image!, enterpriseID: self.post.enterprise!.ID!, points: points)
    }
    
}

extension ShareViewController: ShareDelegate {
    func didShare(_ error: Error?, timeError: Bool, privacyError: Bool) {
        self.waitingView.isHidden = true
        self.waitActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        if let error = error {
            let alert = UIAlertController(title: "Ops", message: "Não foi possível compartilhar no seu mural do Facebook, tente mais tarde!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                self.performSegue(withIdentifier: "unwind", sender: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            print("Erro ao compartilhar: \(error.localizedDescription)")
            return
        }
        
        if timeError {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            let dateString = formatter.string(from: User.instance.lastPosted!.addingTimeInterval(6*60*60))
            let alert = UIAlertController(title: "Ops", message: "Você já compartilhou recentemente, espere até \(dateString)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                self.performSegue(withIdentifier: "unwind", sender: nil)
            }))

            self.present(alert, animated: true, completion: nil)
        }
        
        if privacyError {
            let alert = UIAlertController(title: "Ops", message: "Você não compartilhou em modo público! Vá nas configurações do seu Facebook e altere as permissões do \"Desconta aê!\" para compartilhar em modo \"Público\" e tente novamente.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                self.performSegue(withIdentifier: "unwind", sender: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        let alert = UIAlertController(title: "Sucesso!", message: "Compartilhado com sucesso no seu mural do Facebook! Você ganhou \(points) pontos neste estabelecimento!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            self.performSegue(withIdentifier: "unwind", sender: nil)
        }))
        
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension ShareViewController: GetFriendsCountDelegate {
    func didGetFriendsCount(error: Bool) {
        
        if !error {
            let facebookService = FacebookService()
            userReach = facebookService.calculateUserReach()
            informationLabel.text = "Adicione o texto que você gostaria de ser compartilhado junto com a imagem de propaganda abaixo e compartilhe para ganhar \(userReach/3) pontos baseado no alcance de seu perfil. Dica: para obter \(userReach) pontos, clique em \"Copiar o texto de propaganda\" e cole na caixa de texto."
            
            self.waitingView.isHidden = true
            self.waitActivity.stopAnimating()
        }
        
    }
    
    
}
