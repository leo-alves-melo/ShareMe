//
//  EnterpriseInfoViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 17/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import MessageUI
import ContactsUI

class EnterpriseInfoViewController: UIViewController {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var adressLabel: UILabel!
    
    var enterprise:Enterprise!
    var post:Post!
    
    var store = CNContactStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        descriptionLabel.text = enterprise.info!
        phoneButton.setTitle(enterprise.phone!, for: .normal)
        emailButton.setTitle(enterprise.email!, for: .normal)
        adressLabel.text = enterprise.adress! + ", " + enterprise.city! + ", " + enterprise.state!
        
    }
    
    @IBAction func phoneButtonDidPressed(_ sender: Any) {
        let contact = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        contact.popoverPresentationController?.sourceView = phoneButton
        contact.popoverPresentationController?.sourceRect = phoneButton.bounds
        
       
        contact.addAction(UIAlertAction(title: "Ligar \(enterprise.phone!)", style: .default, handler: callHandler))
        contact.addAction(UIAlertAction(title: "Criar contato", style: .default, handler: createHandler))
        contact.addAction(UIAlertAction(title: "Copiar número", style: .default, handler: numberHandler))
        
        contact.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(contact, animated: true, completion: nil)
        
    }
    
    private func formatPhone(phone:String) -> String {
        var formatedPhone = phone.components(separatedBy: "(")[1].components(separatedBy: ") ")[0]
        formatedPhone += phone.components(separatedBy: ") ")[1].components(separatedBy: "-")[0]
        formatedPhone += phone.components(separatedBy: "-")[1]
        
        return formatedPhone
    }
    
    func callHandler(selectedOption: UIAlertAction) {
        let url: NSURL = URL(string: "TEL://" + formatPhone(phone: enterprise.phone!))! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    func createHandler(selectedOption: UIAlertAction) {
        switch CNContactStore.authorizationStatus(for: .contacts){
        case .authorized:
            self.createContact()
        case .notDetermined:
            self.store.requestAccess(for: .contacts){succeeded, err in
                guard err == nil && succeeded else{
                    return
                }
                self.createContact()
            }
        default:
            print("Not handled")
        }
    }
    
    @IBAction func emailButtonDidPressed(_ sender: Any) {
        let contact = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        contact.popoverPresentationController?.sourceView = emailButton
        contact.popoverPresentationController?.sourceRect = emailButton.bounds
        
        
        
        contact.addAction(UIAlertAction(title: "Enviar e-mail", style: .default, handler: sendEmail))
        contact.addAction(UIAlertAction(title: "Copiar e-mail", style: .default, handler: copyEmail))
        
        contact.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(contact, animated: true, completion: nil)
    }
    
    private func createContact() {
        let contactData = CNMutableContact()
        
        contactData.givenName = enterprise.name!
        
        let workPhone = CNLabeledValue(label: CNLabelWork, value: CNPhoneNumber(stringValue: enterprise.phone!))
        
        contactData.phoneNumbers = [workPhone]
        
        let request = CNSaveRequest()
        request.add(contactData, toContainerWithIdentifier: nil)
        
        do {
            try self.store.execute(request)
            let alert = UIAlertController(title: "Sucesso", message: "O estabelecimento foi adicionado aos seus contatos", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        catch let _ {
            let alert = UIAlertController(title: "Erro", message: "Ocorreu algum problema. Tente novamente", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func numberHandler(selectedOption: UIAlertAction) {
        UIPasteboard.general.string = formatPhone(phone: enterprise.phone!)
    }
    
    private func copyEmail(selectedOption: UIAlertAction) {
        UIPasteboard.general.string = enterprise.email!
    }
    
    private func sendEmail(selectedOption: UIAlertAction) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            let alert = UIAlertController(title: "Erro", message: "Seu e-mail não está configurado em seu dispositivo!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([enterprise.email!])
        composeVC.setSubject("")
        composeVC.setMessageBody("", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
}

extension EnterpriseInfoViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        
        if(result.rawValue == 2) {
            controller.dismiss(animated: true, completion: {
                
                
                let alert = UIAlertController(title: "Sucesso", message: "Seu e-mail foi enviado", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            })
        }
        else {
            controller.dismiss(animated: true, completion: {
                
                
                let alert = UIAlertController(title: "Operação cancelada", message: "Seu e-mail não foi enviado", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            })
        }
        
        
    }
}
