//
//  EnterpriseProfileViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 15/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Firebase

class EnterpriseProfileViewController:UIViewController {
    var enterprise:Enterprise!
    
    @IBOutlet weak var couponsTableView: UITableView!
    @IBOutlet weak var enterpriseImageView: UIImageView!
    @IBOutlet weak var enterpriseNameLabel: UILabel!
    @IBOutlet weak var coinsNumber: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var showInfoButton: UIButton!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var waitActivity: UIActivityIndicatorView!
    @IBOutlet weak var nonCouponFoundLabel: UILabel!
    
    var couponsList:[Coupon] = []
    
    var didReceiveShare:Bool = false
    var didReceiveCoupon:Bool = false
    var didFinish:Bool = false
    
    var postService:PostService!
    var post:Post!
    
     var numberOfCoins = 0
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if didFinish {
            updateUserCoinsLabel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if User.instance.firstName == nil {
            let userService = UserService(delegate: self)
            userService.retrieveUser(ID: Auth.auth().currentUser!.uid)
        }
        else {
            updateUserCoinsLabel()
            
        }
        
        self.nonCouponFoundLabel.isHidden = true
        
        self.waitingView.isHidden = true
        
        couponsTableView.tableFooterView = UIView()
        
        self.imageActivity.startAnimating()
        
        
        self.navigationItem.title = "Estabelecimento"
        self.navigationItem.rightBarButtonItem?.customView?.alpha = 0
        enterpriseImageView.layer.cornerRadius = enterpriseImageView.layer.frame.width/2
        enterpriseImageView.layer.borderWidth = 2
        enterpriseImageView.layer.borderColor = UIColor.white.cgColor
        enterpriseImageView.clipsToBounds = true
        indicateActivityShareView()
        enterpriseNameLabel.text = enterprise.name!

        postService = PostService(delegate: self)
        postService.retrievePost(enterpriseID: enterprise.ID!)
        
        let enterpriseService = EnterpriseService(delegate: self)
        enterpriseService.retrieveEnterpriseImage(enterprise: enterprise)
        
        let cuponsService = CouponService(delegate: self)
        cuponsService.retireveCoupon(enterprise: enterprise)
        
        couponsTableView.estimatedRowHeight = 103
        couponsTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func updateUserCoinsLabel() {
        if let userCoins = User.instance.coin {
            for coin in userCoins {
                if coin.ID! == enterprise.ID! {
                    numberOfCoins = coin.quantity!
                }
            }
        }
        
        self.coinsNumber.text = "Meus pontos: \(numberOfCoins)"
    }
    
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}
    
    @IBAction func ShareButtonDidPressed(_ sender: Any) {
        if didReceiveShare {
            
            performSegue(withIdentifier: "Share", sender: nil)
        }
    }
    
    func finish() {
        if didReceiveShare && didReceiveCoupon {
            self.couponsTableView.isHidden = false
            self.activityIndicator.isHidden = true
            didFinish = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segue = segue.destination as? EnterpriseInfoViewController {
            segue.enterprise = self.enterprise
            segue.post = self.post
        }
        if let segue = segue.destination as? ShareViewController {
            segue.post = self.post
        }
    }
    
    func indicateActivityShareView() {
        self.couponsTableView.isHidden = true
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    @IBAction func InfoButtonDidPressed(_ sender: UIBarButtonItem) {
        
        if didFinish {
            performSegue(withIdentifier: "ShowInfo", sender: nil)
        }
        
    
    }
    
}

extension EnterpriseProfileViewController: PostServiceDelegate {
    func didReceivePostImage(image: UIImage) {
        post.imageLinked!.image = image
        self.didReceiveShare = true
        finish()
    }
    
    func didReceivePost(post: Post) {
        
        self.post = post
        self.postService.retrievePostImage(path: post.imageLinked!.link!)
    }
    
    
}

extension EnterpriseProfileViewController: EnterpriseServiceDelegate {
    func didReceiveEnterpriseImage(image: UIImage) {
        self.enterpriseImageView.image = image
    self.enterpriseImageView.adjustsImageSizeForAccessibilityContentSizeCategory = false

        self.enterprise.imageLinked!.image = image
        
        self.imageActivity.stopAnimating()
        self.imageActivity.isHidden = true
    }
    
    func didReceiveEnterprises(enterprises: [Enterprise]) {
        
    }
    
    
}

extension EnterpriseProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTableViewCell", for: indexPath) as! CouponTableViewCell
        
        cell.nameLable.text = couponsList[indexPath.row].name!
        cell.descriptionLabel.text = couponsList[indexPath.row].detail!
        cell.priceLabel.text = "Preço: \(couponsList[indexPath.row].price!) pontos"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Cupons disponíveis"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let coupon = couponsList[indexPath.row]
        
        
        
        
        if numberOfCoins >= coupon.price! {
            let alert = UIAlertController(title: "Confirme", message: "Gostaria de comprar o cupom \"\(coupon.name!)\"?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Não", style: .default, handler: { (action) in
                //execute some code when this option is selected

            }))
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
                
                self.waitingView.isHidden = false
                self.waitActivity.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
                
                let couponService = CouponService(delegate: self)
                couponService.addCouponToUser(coupon: coupon)
                
                


            }))

            self.present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Ops", message: "Você não tem pontos suficientes para comprar o cupom \"\(coupon.name!)\". Clique no botão de \"Compartilhar\" para adquirir mais pontos!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //execute some code when this option is selected

            }))


            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}

extension EnterpriseProfileViewController: CouponServiceDelegate {
    func didAddCouponToUser(error: Bool, untransferible: Bool, noMoreCoupon: Bool) {
        
        self.waitingView.isHidden = true
        self.waitActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        if error {
            
            if noMoreCoupon {
                let alert = UIAlertController(title: "Ops", message: "Não há mais deste cupom, tente outro!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else if untransferible {
                let alert = UIAlertController(title: "Ops", message: "Você já possui este cupom! Compre outro!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else {
                let alert = UIAlertController(title: "Ops", message: "Não foi possível comprar este cupom agora, tente novamente mais tarde!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else {
            let alert = UIAlertController(title: "Sucesso!", message: "Você acabou de comprar o cupom! Veja ele no seu perfil!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            
            
            self.present(alert, animated: true, completion: nil)
            
            updateUserCoinsLabel()
        }
    }
    
    func didReceiveCoupon(coupons: [Coupon]) {
        
        for coupon in coupons {
            if coupon.quantity! > 0 {
                self.couponsList.append(coupon)
            }
            else {
                self.nonCouponFoundLabel.text = "Não há cupons disponíveis para este estabelecimento"
                self.nonCouponFoundLabel.isHidden = false
            }
        }
        
        
        self.couponsTableView.reloadData()
        didReceiveCoupon = true
        finish()
        
    }
    
    
}

extension EnterpriseProfileViewController: UserServiceDelegate {
    func didReceiveUser() {
        updateUserCoinsLabel()
    }
    
    func didUploadUser(error: Error?) {
        
    }
    
    
}

