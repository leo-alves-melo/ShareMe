//
//  ViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 05/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKShareKit

class ViewController: UIViewController {
    
    
    
    var userID: String!
    
    let facebookService = FacebookService()
    
    @IBOutlet weak var textField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.facebookService.getUserInformations()
        let enterpriseService = EnterpriseService(delegate: self)
        
        //enterpriseService.retrieveEnterprises(category: "restaurant")
        
        let userService = UserService(delegate: self)
        //userService.retrieveUser(ID: userID)
        
        // Do any additional setup after loading the view, typically from a nib.
        
         //print(FBSDKAccessToken.current().tokenString)
        
    }

    @IBAction func read(_ sender: Any) {
        let parameters = ["fields": "total_count", "summary":true] as [String : Any]
        FBSDKGraphRequest(graphPath: "/1979378508745012_1985200328162830/likes", parameters: parameters).start { (connection, result, error) in
            

            
            
            if let error = error {
                print("erro: \(error.localizedDescription)")
                return
            }
            
           
            
            if let result = result as? [String: AnyObject] {
                print("result: \(result)")
                if let data = result["data"] as? [String: AnyObject] {
                    
                    print("1")
                    print(data)
                    
                    
                    
                    
                    
                }
                if let data = result["data"] as? NSDictionary {
                    print("2")
                    print(data)
                }
                if let data = result["data"] as? NSArray {
                    print("3")
                    print(data)
                }
                if let data = result["data"] as? [NSDictionary] {
                    print("4")
                    print(data)
   
                }
            }
        }
    }
    
    @IBAction func share(_ sender: UIButton) {
        
//        let content = facebookService.createLinkShareContent(link: "https://gerunicamp.com.br")
//
//
//        do {
//            let shareDialog = ShareDialog(content: content)
//            shareDialog.mode = .automatic
//            shareDialog.failsOnInvalidData = true
//            shareDialog.completion = { result in
//                print(result)
//                if let result = result as? NSDictionary {
//                    print("Meu id: \(result["id"] as! String)")
//                }
//            }
//            try shareDialog.show()
//        }
//        catch {
//
//        }
        
        
//        do {
//            let sharer = GraphSharer(content: content)
//            sharer.failsOnInvalidData = true
//            sharer.completion = { result in
//
//            }
//
//            try sharer.share()
//        }
//        catch {
//
//        }
        
        
        
        
        
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        

}

extension ViewController: EnterpriseServiceDelegate {
    func didReceiveEnterpriseImage(image: UIImage) {
        
    }
    
    func didReceiveEnterprises(enterprises: [Enterprise]) {
        for enterprise in enterprises {
            enterprise.present()
        }
    }
}

extension ViewController: UserServiceDelegate {
    func didUploadUser(error: Error?) {
        
    }
    
    func didReceiveUser() {
        User.instance.present()
    }
    
    
}
