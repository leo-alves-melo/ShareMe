//
//  ExitTableViewCell.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 24/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class ExitTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
}
