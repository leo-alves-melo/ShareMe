//
//  CouponDetailTableViewCell.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 24/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class CouponDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var shelfLife: UILabel!
    @IBOutlet weak var verificationCode: UILabel!
    
}
