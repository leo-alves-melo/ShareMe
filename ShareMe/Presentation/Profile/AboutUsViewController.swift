//
//  AboutUsViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 24/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class AboutUsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.layer.cornerRadius = imageView.frame.width/2
        self.imageView.layer.masksToBounds = true
        
    }
}
