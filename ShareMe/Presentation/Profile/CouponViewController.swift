//
//  CouponViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 19/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class CouponViewController: UIViewController {
    
    var coupon:Coupon!
    var enterprise:Enterprise!
    var qrcodeImage: CIImage!
    
    var userImage:UIImage!
    var verificationCode:String!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let alert = UIAlertController(title: "Pronto!", message: "Guarde bem este cupom pois é sua garantia do benefício! Você não poderá vê-lo novamente.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        UIGraphicsBeginImageContextWithOptions(tableView.contentSize, true, 0.0)
        
        let savedContentOffset = tableView.contentOffset
        let savedFrame = tableView.frame
        
        tableView.contentOffset = CGPoint.zero
        tableView.frame = CGRect(x: 0, y: 0, width: tableView.contentSize.width, height: tableView.contentSize.height)
        
        tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        tableView.contentOffset = savedContentOffset
        tableView.frame = savedFrame
        
        UIGraphicsEndImageContext()
        
        
        
        if let img = image {
            let objectsToShare = [img] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

extension CouponViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            let couponDate = formatter.date(from: coupon.date!)!
            let newDate = couponDate.addingTimeInterval(TimeInterval(coupon.shelfLife! * 60 * 60))
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponDetail") as! CouponDetailTableViewCell
            cell.title.text = coupon.name!
            cell.detail.text = coupon.detail!
            cell.shelfLife.text = "Válido até \(formatter.string(from: newDate))"
            cell.verificationCode.text = "Código de verificação: \(self.verificationCode!)"
            
            
            cell.selectionStyle = .none
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserDetail") as! UserDetailTableViewCell
            
            if let email = User.instance.email {
                cell.email.text = "E-mail: \(email)"
            }
            else {
                cell.email.text = " "
            }
            
            if let phone = User.instance.phone {
                cell.phone.text = "Telefone: \(phone)"
            }
            else {
                cell.phone.text = " "
            }
            
            cell.name.text = "\(User.instance.firstName!) \(User.instance.lastName!)"
            
            cell.photo.image = userImage
            cell.photo.layer.cornerRadius = cell.photo.frame.width/2
            cell.photo.layer.masksToBounds = true
            
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseDetail") as! EnterpriseDetailTableViewCell
            
            cell.name.text = enterprise.name!
            cell.info.text = enterprise.info!
            cell.adress.text = "\(enterprise.adress!), \(enterprise.city!), \(enterprise.state!)."
            cell.email.text = "E-mail: \(enterprise.email!)"
            cell.phone.text = "Telefone: \(enterprise.phone!)"
            cell.enterpriseImage.image = enterprise.imageLinked?.image
            
            cell.enterpriseImage.layer.cornerRadius = (cell.enterpriseImage.frame.width)/2
            cell.enterpriseImage.layer.masksToBounds = true
            
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Cupom"
        }
        else if section == 1 {
            return "Beneficiário"
        }
        else {
            return "Estabelecimento"
        }
    }
}
