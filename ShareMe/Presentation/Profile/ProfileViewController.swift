//
//  ProfileViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 15/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController {
    
    var couponsList:[Coupon]!
    var coinsList:[Coin]!
    var enterpriseListCoupons:[Enterprise] = []
    var enterpriseListCoins:[Enterprise] = []
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    @IBOutlet weak var waitView: UIView!
    @IBOutlet weak var waitActivity: UIActivityIndicatorView!
    @IBOutlet weak var notFoundLabel: UILabel!
    
    private var coinEnterprises = 0
    private var couponEnterprises = 0
    private var couponCount = 0
    private var enterpriseService:EnterpriseService?
    private var couponsService:CouponService?
    private var selectedCoupon:Coupon!
    private var didFinish:Bool = false
    private var verificationCode:String? = nil
    private var emailSend = false
    
    var enterprise:Enterprise? = nil
    
    var finishCoupon = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        waitView.isHidden = true
        
        self.profileName.text = ""
        self.profileTableView.isHidden = true
        self.activityIndicator.startAnimating()
        self.imageActivity.startAnimating()
        
        profileTableView.tableFooterView = UIView()
        
        self.notFoundLabel.isHidden = true
        
        
        self.profileImageView.layer.cornerRadius = self.profileImageView.layer.frame.width/2
        self.profileImageView.layer.borderWidth = 2
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.clipsToBounds = true
        
        self.profileTableView.estimatedRowHeight = 95
        self.profileTableView.rowHeight = UITableViewAutomaticDimension
        
        self.enterpriseService = EnterpriseService(delegate: self)
        self.couponsService = CouponService(delegate: self)
        
        
        
        if User.instance.firstName == nil {
            let userService = UserService(delegate: self)
            userService.retrieveUser(ID: Auth.auth().currentUser!.uid)
        }
        else {
            initiate()
        
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //Carrega novamente os dados da tableView
        if didFinish {
            coinEnterprises = 0
            couponEnterprises = 0
            couponCount = 0
            couponsList = []
            coinsList = []
            enterpriseListCoupons = []
            enterpriseListCoins = []
            self.profileTableView.reloadData()
            finishCoupon = false
            didFinish = false
            self.notFoundLabel.isHidden = true
            self.profileTableView.isHidden = true
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.enterprise = nil
            self.emailSend = false
            initiate()
            
        }
        
        
    }
    
    private func randomString(length: Int) -> String {
        
        let letters : NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func initiate() {
        
        self.profileName.text = User.instance.firstName!
        
        let facebookService = FacebookService()
        facebookService.getProfileImageDelegate = self
        facebookService.getProfileImage()
        
        
        
        
        if User.instance.coin != nil {
            coinsList = User.instance.coin!
        }
        else {
            coinsList = []
        }
        
        if User.instance.coupon != nil {
            couponsList = User.instance.coupon!
        }
        else {
            couponsList = []
            finishCoupon = true
        }
        
        if self.couponsList.count > 0 {
            
            couponsService?.retrieveCoupon(ID: couponsList[0].ID!)
        }
        else {
            finishCoupon = true
            if self.coinsList.count > 0 {
                enterpriseService?.retrieveEnterprise(ID: coinsList[0].ID!)
            }
            else {
                didFinish = true
                self.activityIndicator.isHidden = true
                if segmentedControl.selectedSegmentIndex == 0 {
                    
                    self.notFoundLabel.isHidden = false
                    self.notFoundLabel.text = "Você ainda não tem cupons!"
                }
                    
                else if segmentedControl.selectedSegmentIndex == 1 {
                
                    self.notFoundLabel.isHidden = false
                    self.notFoundLabel.text = "Você ainda não tem pontos!"
                    
                }
            }
        }
        

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segue = segue.destination as? EnterpriseProfileViewController {
            segue.enterprise = self.enterprise
        }
        else if segue.identifier == "Logout" {
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                print("Deslogado do Firebase")
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        }
        else if let segue = segue.destination as? CouponViewController {
            segue.coupon = self.selectedCoupon
            segue.enterprise = self.enterprise
            segue.userImage = self.profileImageView.image
            segue.verificationCode = self.verificationCode!
        }
        
    }
    
    @IBAction func changeSegmentedIndex(_ sender: UISegmentedControl) {
        
        if didFinish {
            if segmentedControl.selectedSegmentIndex == 0 {
                if couponsList.count == 0  {
                    self.notFoundLabel.isHidden = false
                    self.notFoundLabel.text = "Você ainda não tem cupons!"
                }
                else {
                    self.notFoundLabel.isHidden = true
                }
            }
                
            else if segmentedControl.selectedSegmentIndex == 1 {
                if coinsList.count == 0 {
                    self.notFoundLabel.isHidden = false
                    self.notFoundLabel.text = "Você ainda não tem pontos!"
                }
                else {
                    self.notFoundLabel.isHidden = true
                }
            }
            
            self.profileTableView.reloadData()
        }
        
    }
    
    
    func finishCouponFunc() {
        finishCoupon = true
        enterpriseService?.retrieveEnterprise(ID: couponsList[0].enterprise!)
        
    }
    
    func finish() {
        if couponCount == couponsList.count && couponEnterprises == couponsList.count && coinEnterprises == coinsList.count {
            
            
            self.profileTableView.isHidden = false
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            profileTableView.reloadData()
            didFinish = true
            
            if couponsList.count == 0 && segmentedControl.selectedSegmentIndex == 0 {
                self.notFoundLabel.isHidden = false
                self.notFoundLabel.text = "Você ainda não tem cupons!"
            }
            else if coinsList.count == 0 && segmentedControl.selectedSegmentIndex == 1 {
                self.notFoundLabel.isHidden = false
                self.notFoundLabel.text = "Você ainda não tem pontos!"
            }
            
            
        }
    }
    
    
    func finishCouponSelected() {
        
        if self.enterprise != nil {
            if self.enterprise!.imageLinked?.image != nil && self.profileImageView.image != nil && emailSend {
                
                
                let userService = UserService(delegate: self)
                userService.updateUserInFirebase()
            }
        }
        
    }
}

extension ProfileViewController: CouponServiceDelegate {
    func didAddCouponToUser(error: Bool, untransferible: Bool, noMoreCoupon: Bool) {
        
    }
    
    func didReceiveCoupon(coupons: [Coupon]) {
        couponsList[couponCount].detail = coupons[0].detail!
        couponsList[couponCount].name = coupons[0].name!
        couponsList[couponCount].enterprise = coupons[0].enterprise!
        couponsList[couponCount].shelfLife = coupons[0].shelfLife!
        couponCount += 1
        
        if couponCount < couponsList.count {
            couponsService?.retrieveCoupon(ID: couponsList[couponCount].ID!)
        }
        else {
            finishCouponFunc()
        }
    }
    

    
    
}

extension ProfileViewController: GetProfileImageDelegate {
    func didGetProfileImage(image: UIImage) {
        self.profileImageView.image = image
        self.imageActivity.stopAnimating()
        self.imageActivity.isHidden = true
        finishCouponSelected()
        
    }
    
    
}

extension ProfileViewController: EnterpriseServiceDelegate {
    func didReceiveEnterprises(enterprises: [Enterprise]) {
        
        if finishCoupon {
            if couponEnterprises < couponsList.count {
                couponEnterprises += 1
                
                if couponEnterprises < couponsList.count {
                    enterpriseService?.retrieveEnterprise(ID: couponsList[couponEnterprises].enterprise!)
                }
                else {
                    enterpriseService?.retrieveEnterprise(ID: coinsList[coinEnterprises].ID!)
                }
                
                self.enterpriseListCoupons.append(enterprises[0])
                
                
            }
            else {
                coinEnterprises += 1
                
                self.enterpriseListCoins.append(enterprises[0])
                
                if coinEnterprises < coinsList.count {
                    enterpriseService?.retrieveEnterprise(ID: coinsList[coinEnterprises].ID!)
                }
                else {
                    finish()
                }
                
                
                
                
            }
        }
        
        
    }
    
    func didReceiveEnterpriseImage(image: UIImage) {
        self.enterprise!.imageLinked?.image = image
        finishCouponSelected()
    }
    
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.segmentedControl.selectedSegmentIndex == 0 {
            return self.enterpriseListCoupons.count
        }
        else {
            return self.enterpriseListCoins.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        
        //Cupons
        if self.segmentedControl.selectedSegmentIndex == 0 {
            
            //Calcula validade do cupom
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            let couponDate = formatter.date(from: self.couponsList[indexPath.row].date!)!
            let newDate = couponDate.addingTimeInterval(TimeInterval(self.couponsList[indexPath.row].shelfLife! * 60 * 60))
            
            
            
            
            cell.nameLabel.text = "\(self.couponsList[indexPath.row].name!)"
            cell.detailLabel.text = "\(self.couponsList[indexPath.row].detail!)\n\nVálido até: \(formatter.string(from: newDate))"
            cell.otherDetailLabel.text = "\(enterpriseListCoupons[indexPath.row].name!), \(enterpriseListCoupons[indexPath.row].city!), \(enterpriseListCoupons[indexPath.row].state!)"
        }
        //Pontos
        else {
            cell.nameLabel.text = "\(enterpriseListCoins[indexPath.row].name!), \(enterpriseListCoins[indexPath.row].city!), \(enterpriseListCoins[indexPath.row].state!)"
            cell.detailLabel.text = "\(self.coinsList[indexPath.row].quantity!) pontos"
            cell.otherDetailLabel.text = self.enterpriseListCoins[indexPath.row].info!
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            let alert = UIAlertController(title: "Confirme", message: "Deseja utilizar agora o cupom \"\(couponsList[indexPath.row].name!)\"?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Não", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
                //execute some code when this option is selected
                self.selectedCoupon = self.couponsList[indexPath.row]
                self.enterprise = self.enterpriseListCoupons[indexPath.row]
                
                let enterpriseService = EnterpriseService(delegate: self)
                enterpriseService.retrieveEnterpriseImage(enterprise: self.enterprise!)
                
                User.instance.coupon!.remove(at: indexPath.row)
                self.couponsList.remove(at: indexPath.row)
                self.enterpriseListCoupons.remove(at: indexPath.row)
                
                self.waitView.isHidden = false
                self.waitActivity.startAnimating()
                
                self.verificationCode = self.randomString(length: 6)
                
                FirebaseService.instance.activateFunctionDelegate = self
                FirebaseService.instance.couponDidUsed(coupon: self.selectedCoupon, enterprise: self.enterprise!, verificationCode: self.verificationCode!)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            self.enterprise = enterpriseListCoins[indexPath.row]
            performSegue(withIdentifier: "EnterpriseSegue", sender: nil)
        }
    }
    
    
}

extension ProfileViewController: LogoutDelegate {
    func didLogout(error: Error?) {
        
        performSegue(withIdentifier: "Logout", sender: nil)
    }
    
    
}

extension ProfileViewController: UserServiceDelegate {
    func didReceiveUser() {
        initiate()
    }
    
    func didUploadUser(error: Error?) {
        self.waitView.isHidden = true
        self.waitActivity.stopAnimating()
        profileTableView.reloadData()
        self.performSegue(withIdentifier: "CouponSelected", sender: nil)
    }
    
    
}

extension ProfileViewController: ActivateFunctionDelegate {
    func didActivateFunction(error: Bool) {
        if error {
            
        }
        else {
            self.emailSend = true
            finishCouponSelected()
        }
    }
    
    
}
