//
//  SettingsViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 23/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import MessageUI

class SettingsViewController: UIViewController {
    
    let actions = ["Cadastrar meu estabelecimento"]
    
    let information = ["Como funciona o App", "Sobre nós"]
    
    let exit = ["Sair"]
    
    let enterpriseEmail = "descontaaeapp@gmail.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func singOut() {
        let alert = UIAlertController(title: "Confirme", message: "Deseja sair do aplicativo?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
            let facebookService = FacebookService()
            facebookService.logoutDelegate = self
            facebookService.logout()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Não", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func wantToEmail(_ sender: Any) {
        let contact = UIAlertController(title: "Nos contate!", message: "Envie-nos um e-mail para saber como cadastrar seu estabelecimento.", preferredStyle: .actionSheet)
        
        if let sender = sender as? UITableViewCell {
            contact.popoverPresentationController?.sourceRect = sender.contentView.layer.contentsRect
            contact.popoverPresentationController?.sourceView = sender.contentView
        }
        
        
        
        
        contact.addAction(UIAlertAction(title: "Enviar e-mail", style: .default, handler: sendEmail))
        contact.addAction(UIAlertAction(title: "Copiar e-mail", style: .default, handler: copyEmail))
        
        contact.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(contact, animated: true, completion: nil)
    }
    
    private func copyEmail(selectedOption: UIAlertAction) {
        UIPasteboard.general.string = self.enterpriseEmail
    }
    
    private func sendEmail(selectedOption: UIAlertAction) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            let alert = UIAlertController(title: "Erro", message: "Seu e-mail não está configurado em seu dispositivo!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([enterpriseEmail])
        composeVC.setSubject("Gostaria de cadastrar meu estabelecimento")
        composeVC.setMessageBody("Olá equipe do Desconta aê! Eu gostaria de cadastrar meu estabelecimento, como devo proceder?", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
}

extension SettingsViewController: LogoutDelegate {
    func didLogout(error: Error?) {
        performSegue(withIdentifier: "Logout", sender: nil)
    }
    
    
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return actions.count
        }
        else if section == 1 {
            return information.count
        }
        else {
            return exit.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CleanTableViewCell", for: indexPath as IndexPath) as! CleanTableViewCell
            cell.titleLabel.text = actions[indexPath.row]
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath as IndexPath) as! SettingsTableViewCell
            cell.title!.text = information[indexPath.row]
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExitTableViewCell", for: indexPath as IndexPath) as! ExitTableViewCell
            cell.titleLabel.text = exit[indexPath.row]
            return cell
        }

        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Ações"
        }
        else if section == 1 {
            return "Informações"
        }
        else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 0 {
            //Cadastrar estabelecimento
            if indexPath.row == 0 {
                wantToEmail(tableView.cellForRow(at: indexPath)!)
            }
        }
        else if indexPath.section == 1 {
            //Como funciona o App
            if indexPath.row == 0 {
                performSegue(withIdentifier: "AboutApp", sender: nil)
            }
            //Sobre nos
            if indexPath.row == 1 {
                performSegue(withIdentifier: "AboutUs", sender: nil)
            }
        }
            //Sair
        else {
            self.singOut()
        }
    }
    
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        
        if(result.rawValue == 2) {
            controller.dismiss(animated: true, completion: {
                
                
                let alert = UIAlertController(title: "Sucesso", message: "Seu e-mail foi enviado", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            })
        }
        else {
            controller.dismiss(animated: true, completion: {
                
                
                let alert = UIAlertController(title: "Operação cancelada", message: "Seu e-mail não foi enviado", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            })
        }
        
        
    }
}
