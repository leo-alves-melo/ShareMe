//
//  EnterpriseDetailTableViewCell.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 24/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class EnterpriseDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    
}
