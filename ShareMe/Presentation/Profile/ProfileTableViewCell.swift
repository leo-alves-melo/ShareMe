//
//  ProfileTableViewCell.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 18/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var otherDetailLabel: UILabel!
    
}
