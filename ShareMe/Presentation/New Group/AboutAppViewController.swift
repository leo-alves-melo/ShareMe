//
//  AboutAppViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 23/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation


class AboutAppViewController:UIViewController {
    
    @IBOutlet weak var imageBackground: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageBackground.layer.cornerRadius = imageBackground.frame.width/5
        self.imageBackground.layer.masksToBounds = true
    }
    

}
