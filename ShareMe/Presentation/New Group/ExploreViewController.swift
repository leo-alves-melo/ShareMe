//
//  ExploreViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 15/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import Firebase

class ExploreViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var categoriesView: UIView!
    
    @IBOutlet weak var restaurantsButton: UIButton!
    @IBOutlet weak var barsButton: UIButton!
    @IBOutlet weak var funButton: UIButton!
    @IBOutlet weak var storesButton: UIButton!
    @IBOutlet weak var serviceButton: UIButton!
    @IBOutlet weak var othersButton: UIButton!
    
    var enterpriseList:[Enterprise] = []
    var filteredEnterpriseList:[Enterprise] = []
    
    var activityIndicator:UIActivityIndicatorView? = nil
    var noneEnterpricesFound:UILabel? = nil
    
    var didGetAllEnterprises:Bool = false
    
    var enterprise:Enterprise? = nil
    
    var returnedFromAnotherViewController = false
    
    let enterpriseCategories = ["restaurant", "bar", "fun", "store", "services", "others"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.categoriesView.addGestureRecognizer(tap)
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if returnedFromAnotherViewController {
            //self.searchBar.showsCancelButton = true
            self.returnedFromAnotherViewController = false
            showsCancelButton()
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func showsCancelButton() {
        searchBar.showsCancelButton = true
        for view1 in searchBar.subviews {
            for view2 in view1.subviews {
                if view2 is UIButton {
                    let button = view2 as! UIButton
                    button.isEnabled = true
                    button.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    private func indicateActivity() {
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator!.center = view.center
        activityIndicator!.startAnimating()
        view.addSubview(activityIndicator!)
    }
    
    func pressCategoryButton(button: UIButton) {
        showsCancelButton()
        
        if !didGetAllEnterprises {
            let enterpriseService = EnterpriseService(delegate: self)
            enterpriseService.retrieveEnterprises(category: enterpriseCategories[button.tag])
            self.categoriesView.isHidden = true
            indicateActivity()
        }
        else {
            filterByCategory(enterpriseCategories[button.tag])
            if filteredEnterpriseList.count > 0 {
                self.tableView.isHidden = false
                self.categoriesView.isHidden = true
                self.tableView.reloadData()
            }
            else {
                self.tableView.isHidden = false
                self.categoriesView.isHidden = true
                setNoneCategoriesFound()
            }
            self.searchBar.showsCancelButton = true
        }
    }
    
    @IBAction func restaurantsButtonDidPressed(_ sender: UIButton) {
        pressCategoryButton(button: restaurantsButton)
    }
    
    @IBAction func barButtonDidPressed(_ sender: UIButton) {
        
        pressCategoryButton(button: barsButton)
    }
    
    @IBAction func funButtonDidPressed(_ sender: UIButton) {
        
       pressCategoryButton(button: funButton)
    }
    
    @IBAction func storeButtonDidPressed(_ sender: UIButton) {
        
        pressCategoryButton(button: storesButton)
    }
    
    @IBAction func serviceButtonDidPressed(_ sender: UIButton) {
        
        pressCategoryButton(button: serviceButton)
    }
    
    @IBAction func otherButtonDidPressed(_ sender: UIButton) {
        
        pressCategoryButton(button: othersButton)
    }
    
    func filterByCategory(_ category: String) {
        for enterprise in enterpriseList {
            if enterprise.category!.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).hasPrefix(category.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current)) {
                self.filteredEnterpriseList.append(enterprise)
            }
        }
    }
    
    func setNoneEnterprisesFound() {
        noneEnterpricesFound?.removeFromSuperview()
        self.noneEnterpricesFound = UILabel(frame: CGRect(x: 0, y: 0, width: view.layer.frame.width - 20, height: 42))
        noneEnterpricesFound!.text = "Não encontramos estabelecimentos com este nome"
        noneEnterpricesFound!.center = self.view.center
        noneEnterpricesFound!.textAlignment = .center
        noneEnterpricesFound?.textColor = UIColor.gray
        noneEnterpricesFound!.lineBreakMode = .byWordWrapping
        noneEnterpricesFound!.numberOfLines = 3
        self.view.addSubview(noneEnterpricesFound!)
    }
    
    func setNoneCategoriesFound() {
        noneEnterpricesFound?.removeFromSuperview()
        self.noneEnterpricesFound = UILabel(frame: CGRect(x: 0, y: 0, width: view.layer.frame.width - 20, height: 42))
        noneEnterpricesFound!.text = "Não encontramos estabelecimentos com esta categoria"
        noneEnterpricesFound!.center = self.view.center
        noneEnterpricesFound!.textAlignment = .center
        noneEnterpricesFound?.textColor = UIColor.gray
        noneEnterpricesFound!.lineBreakMode = .byWordWrapping
        noneEnterpricesFound!.numberOfLines = 3
        self.view.addSubview(noneEnterpricesFound!)
    }
    
    func filterByText() {
        
        if let text = self.searchBar.text {
            if text == "" {
                filteredEnterpriseList = enterpriseList
            }
            else {
                for enterprise in enterpriseList {
                    if enterprise.name!.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current).hasPrefix(text.lowercased().folding(options: .diacriticInsensitive, locale: Locale.current)) {
                        self.filteredEnterpriseList.append(enterprise)
                    }
                }
            }
        }
        else {
            filteredEnterpriseList = enterpriseList
        }
        
    }
    
    func getAllEnterprises() {
        if !didGetAllEnterprises {
            let enterpriseService = EnterpriseService(delegate: self)
            enterpriseService.retrieveAllEnterprises()
            indicateActivity()
            self.didGetAllEnterprises = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segue = segue.destination as? EnterpriseProfileViewController {
            segue.enterprise = self.enterprise!
            self.returnedFromAnotherViewController = true
        }
    }
    
}

extension ExploreViewController: EnterpriseServiceDelegate {
    func didReceiveEnterpriseImage(image: UIImage) {
        
    }
    
    func didReceiveEnterprises(enterprises: [Enterprise]) {
        self.enterpriseList = enterprises
        self.activityIndicator!.removeFromSuperview()
        filterByText()
        if filteredEnterpriseList.count > 0 {
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }
        else {
            setNoneCategoriesFound()
        }
        
        
    }
    
    
}

extension ExploreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredEnterpriseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
        
        let enterprise = filteredEnterpriseList[indexPath.row]
        
        cell.enterpriseName.text = enterprise.name! + ", " + enterprise.city! + ", " + enterprise.state!
        
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.enterprise = filteredEnterpriseList[indexPath.row]
        performSegue(withIdentifier: "selectEnterprise", sender: nil)
    }
}

extension ExploreViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tableView.isHidden = true
        self.view.endEditing(true)
        self.searchBar.text = ""
        self.searchBar.showsCancelButton = false
        self.categoriesView.isHidden = false
        noneEnterpricesFound?.removeFromSuperview()
        self.filteredEnterpriseList.removeAll()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange: String) {
        self.searchBar.showsCancelButton = true
        
        if (!textDidChange.isEmpty) {
            
            self.filteredEnterpriseList.removeAll()
            self.categoriesView.isHidden = true
            
            if(self.enterpriseList.count == 0) {
                self.categoriesView.isHidden = true
                
                getAllEnterprises()
            }
            else {
                filterByText()
                
                if filteredEnterpriseList.count == 0 {
                    setNoneEnterprisesFound()
                    self.tableView.isHidden = true
                }
                else {
                    noneEnterpricesFound?.removeFromSuperview()
                    self.tableView.isHidden = false
                    self.categoriesView.isHidden = true
                    self.tableView.reloadData()
                }
                
                
            }
            
        }
        else {
            self.tableView.isHidden = true
            noneEnterpricesFound?.removeFromSuperview()
            self.filteredEnterpriseList.removeAll()
        }
        
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ExploreViewController: UserServiceDelegate {
    func didUploadUser(error: Error?) {
        
    }
    
    func didReceiveUser() {
        
    }
    
    
}

