//
//  LoginViewController.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 08/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import Firebase

class LoginViewController: UIViewController {
    
    let facebookService = FacebookService()
    var userID:String? = nil
    
    let tutorialText:[String] = ["Bem vindo ao Desconta aê!\nVamos começar? Arraste para o lado", "Compartilhe estabelecimentos no seu Facebook para ganhar pontos", "O alcance de seu perfil determinará o número de pontos que você vai ganhar", "Troque estes pontos por cupons do estabelecimento", "Apresente o cupom no estabelecimento para utilizá-lo","Permita que o aplicativo compartilhe no Facebook em modo público.\nRelaxe! Só publicaremos quando você pedir ;)"]
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var facebookButton: UIView!
    
    @IBOutlet weak var waitView: UIView!
    @IBOutlet weak var waitActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        self.waitView.isHidden = true
        self.waitActivityIndicator.isHidden = true
        self.facebookButton.addSubview(self.facebookService.loginButton)
        self.facebookService.loginButton.center = CGPoint(x: self.facebookButton.frame.width/2, y:  self.facebookButton.frame.height/2)
        self.facebookService.loginButton.delegate = self
        
        self.facebookButton.alpha = 0
        
        self.pageControl.numberOfPages = tutorialText.count
        
        //Add all pages in scroolView
        for text in tutorialText {
            
            let label = UILabel(frame: CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: self.view.frame.width - 20, height: self.view.frame.height))
            label.center = view.center
            label.textAlignment = .center
            label.text = text
            label.numberOfLines = 0
            label.textColor = UIColor.white
            
            
            label.frame = (label.frame.offsetBy(dx: scrollView.contentSize.width, dy: 0))
            
            scrollView.addSubview(label)
            
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width + self.view.frame.width, height: label.frame.height)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if var destination = segue.destination as? ViewController {
            destination.userID = self.userID!
        }
    }
    
    
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if let error = error {
            print("erro: \(error.localizedDescription)")
            return
        }
        
        if result.isCancelled {
            
            return
        }
        
        let alert = UIAlertController(title: "Confirme", message: "Para fazer login, é necessário que você permita o acesso ao seu e-mail, publicações e amigos.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action) in
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
        }))
        alert.addAction(UIAlertAction(title: "Permitir", style: .default, handler: { (action) in
            
            let loginManager = FBSDKLoginManager()
            loginManager.logIn(withReadPermissions: ["public_profile","email", "user_posts", "user_friends"], from: self, handler: { (result, error) in
                
                
                if let error = error {
                    print("erro: \(error.localizedDescription)")
                    let loginManager = FBSDKLoginManager()
                    loginManager.logOut()
                    return
                }
                
                if result!.isCancelled {
                    let loginManager = FBSDKLoginManager()
                    loginManager.logOut()
                    return
                }
                
                
                
                
                self.waitView.isHidden = false
                self.waitActivityIndicator.isHidden = false
                self.waitActivityIndicator.startAnimating()
                
                
                
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                Auth.auth().signIn(with: credential) { (user, error) in
                    if let error = error {
                        
                        self.waitView.isHidden = true
                        self.waitActivityIndicator.isHidden = true
                        
                        print("Erro de autenticação do Firebase: \(error.localizedDescription)")
                        let loginManager = FBSDKLoginManager()
                        loginManager.logOut()
                        return
                    }
                    
                    User.instance.ID = Auth.auth().currentUser?.uid
                    
                    //Checa se já existe o usuário, e se não, cria um novo
                    let userService = UserService(delegate: self)
                    userService.retrieveUser(ID: Auth.auth().currentUser!.uid)
                }
            })
        }))
        
        self.present(alert, animated: true, completion: nil)
  
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            print("Deslogado do Firebase")
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        return true
    }
    
    
}

extension LoginViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let page = floor(scrollView.contentOffset.x / self.view.frame.width)
        
        pageControl.currentPage = Int(page)
        
        if(Int(page) == pageControl.numberOfPages - 1) {
            
            UIView.animate(withDuration: 1, animations: {
                self.facebookButton.alpha = 1
            })
        }
    }
}

extension LoginViewController: GetFriendsCountDelegate {
    func didGetFriendsCount(error: Bool) {
        if !error {
            
        }
    }
    
    
}

extension LoginViewController: GetUserDelegate {
    func didReceiveUser(error: Bool) {
        if !error {
            
            let userService = UserService(delegate: self)
            userService.updateUserInFirebase()
            
            
        }
    }
    
    
}



extension LoginViewController: UserServiceDelegate {
    func didReceiveUser() {
        //Se não existir o usuário, cria um novo
        if User.instance.firstName == nil {
            
            //Faz a requisição para os outros dados
            let facebookService = FacebookService()
            facebookService.getUserDelegate = self
            facebookService.getUserInformations()
            
        }
        else {
            self.performSegue(withIdentifier: "Logged", sender: self)
        }
    }
    
    func didUploadUser(error: Error?) {
        
        if error == nil {
            self.performSegue(withIdentifier: "Logged", sender: self)
        }
        
    }
    
    
}



