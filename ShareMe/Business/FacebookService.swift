//
//  FacebookService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 08/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import Social
import FacebookShare
import Firebase


protocol GetUserDelegate {
    func didReceiveUser(error: Bool)
}

protocol ShareDelegate {
    func didShare(_ error:Error?, timeError:Bool, privacyError:Bool)
}

protocol GetFriendsCountDelegate {
    func didGetFriendsCount(error:Bool)
}

protocol GetProfileImageDelegate {
    func didGetProfileImage(image:UIImage)
}

protocol LogoutDelegate {
    func didLogout(error: Error?)
}

protocol DeletePost {
    func didDeletePost(error: Bool)
}

class FacebookService {
    
    var getUserDelegate: GetUserDelegate? = nil
    var shareDelegate: ShareDelegate? = nil
    var getProfileImageDelegate: GetProfileImageDelegate? = nil
    var logoutDelegate:LogoutDelegate? = nil
    var getFriendsCountDelegate: GetFriendsCountDelegate? = nil
    
    private var textToShare:String? = nil
    private var imageToShare:UIImage? = nil
    private var entepriseIDToShare:String? = nil
    private var privacyError = false
    
    var loginButton = FBSDKLoginButton()
    
    var userReach = 0
    
    init() {
        self.loginButton.readPermissions = ["public_profile", "user_friends",  "email", "user_posts"]
        self.loginButton.publishPermissions = ["publish_actions"]
    }
    
    func createPhotoShareContent(image: UIImage) -> PhotoShareContent {
        
        let photo = Photo(image: image, userGenerated: true)
        let content = PhotoShareContent(photos: [photo])

        return content
        
    }
    
    func createLinkShareContent(link: String) -> LinkShareContent {
        
        let content = LinkShareContent(url: NSURL(string: link)! as URL)
        
        return content
        
    }
    
    func logout() {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        let userService = UserService()
        userService.deleteUser()
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            print("Deslogado do Firebase")
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        logoutDelegate?.didLogout(error: nil)
    }
    
    func getProfileImage() {
        
        
        
        
        let pictureURL = "https://graph.facebook.com/\(FBSDKAccessToken.current().userID!)/picture?type=large&return_ssl_resources=1"
        let URLRequest = NSURL(string: pictureURL)
        let URLRequestNeeded = NSURLRequest(url: URLRequest! as URL)
        
        NSURLConnection.sendAsynchronousRequest(URLRequestNeeded as URLRequest, queue: OperationQueue.main, completionHandler: {(response: URLResponse?,data: Data?, error: Error?) -> Void in
            
            if error == nil {
                let picture = UIImage(data: data!)
                self.getProfileImageDelegate?.didGetProfileImage(image: picture!)
                
            }
            else {
                print("Error: \(error!.localizedDescription)")
            }
        })
        
    }
    
    func calculateUserReach() -> Int {
        
        return User.instance.numberOfFriends!/3
    }
    
    private func sendPostedToFirebase(postID:String, enterpriseID:String) {
        
       
        
        
        var added = false
        if User.instance.coin != nil {
            for i in 0..<User.instance.coin!.count {
                if User.instance.coin![i].ID! == enterpriseID {
                    User.instance.coin![i].quantity = User.instance.coin![i].quantity! + userReach
                    added = true
                }
            }
            
            if !added {
                var coin = Coin()
                coin.ID = enterpriseID
                coin.quantity = userReach
                User.instance.coin!.append(coin)
            }
        }
        else {
            var coin = Coin()
            coin.ID = enterpriseID
            coin.quantity = userReach
            User.instance.coin = [coin]
        }
        
        
        User.instance.lastPosted = Date()
       
        let json: NSMutableDictionary = NSMutableDictionary()
        json.setValue(enterpriseID, forKey: "enterpriseID")
        json.setValue(Auth.auth().currentUser!.uid, forKey: "userID")
        json.setValue(postID, forKey: "postID")
        json.setValue(userReach, forKey: "coins")
        
        FirebaseService.instance.uploadJsonDelegate = self
        FirebaseService.instance.uploadJson(json: json as! [String : AnyObject], reference: FirebaseService.instance.postedReference.child(postID))
        
        
        
    }
    
//    func share(text: String, enterpriseID: String) {
//        if FBSDKAccessToken.current().hasGranted("publish_actions") {
//
//            FBSDKGraphRequest.init(graphPath: "me/feed", parameters: ["message" : text], httpMethod: "POST").start(completionHandler: { (connection, result, error) -> Void in
//                if let error = error {
//                    print("Error: \(error)")
//                } else {
//
//                    if let result = result as? NSDictionary {
//
//                        let id = result["id"] as! String
//
//
//
//                        self.sendPostedToFirebase(postID: id, enterpriseID: enterpriseID)
//
//                    }
//                }
//            })
//        } else {
//            print("require publish_actions permissions")
//        }
//    }
    
    func deletePost(postID:String) {
        
        FBSDKGraphRequest.init(graphPath: postID, parameters: nil, httpMethod: "DELETE").start(completionHandler: { (connection, result, error) -> Void in
            if let error = error {
                print("Error: \(error)")
                //TO - DO: Checa se o erro foi que a pessoa já deletou o post anterior
                
                User.instance.lastPostedID = nil
                self.didDeletePost(error: false)
            } else {
                
                User.instance.lastPostedID = nil
                self.didDeletePost(error: false)
            }
        })
    }
    
    func checkPostPrivacy(postID:String) {
        
        let parameters:NSMutableDictionary = NSMutableDictionary()
        
        parameters.setValue("privacy", forKey: "fields")
        
        
        FBSDKGraphRequest.init(graphPath: postID, parameters: parameters as! [AnyHashable : Any], httpMethod: "GET").start(completionHandler: { (connection, result, error) -> Void in
            if let error = error {
                print("Error: \(error)")
            } else {
                
                if let result = result as? NSDictionary {
                    
                    if let privacy = result["privacy"] as? [String:AnyObject] {
                        if let value = privacy["value"] as? String {
                            //Se a pessoa compartilhou com todo mundo ou com todos os amigos
                            if value == "EVERYONE" || value == "ALL_FRIENDS" {
                                self.sendPostedToFirebase(postID: postID, enterpriseID: self.entepriseIDToShare!)
                                
                            }
                            //Se a pessoa não compartilhou público ou para amigos, não dá os pontos para ela
                            else {
                                
                                self.privacyError = true
                                //Atualiza o último post da pessoa no banco de dados
                                let userService = UserService(delegate: self)
                                userService.updateUserInFirebase()
                                
                            }
                            
                        }
                    }
                   
                   
                }
            }
        })
   
    }
    
    
    //A FAZER: apagar o ultimo post da pessoa, assim evita que o Facebook coloque em um Album.
    func share(text: String, image:UIImage, enterpriseID: String, points:Int) {
        
        //Checa se já se passaram 6 horas antes do último post
        if let lastPosted = User.instance.lastPosted {
            
            if lastPosted.addingTimeInterval(6*60*60) > Date() {
                self.shareDelegate?.didShare(nil, timeError: true, privacyError: false)
                return
            }
        }
        
        self.userReach = points
        
        if User.instance.lastPostedID == nil {
            if FBSDKAccessToken.current().hasGranted("publish_actions") {
                
                FBSDKGraphRequest.init(graphPath: "me/photos", parameters: ["caption" : text, "sourceImage": image], httpMethod: "POST").start(completionHandler: { (connection, result, error) -> Void in
                    if let error = error {
                        print("Error: \(error)")
                    } else {
                        
                        if let result = result as? NSDictionary {
                            
                           
                            let id = result["post_id"] as! String
                            User.instance.lastPostedID = id
                            self.entepriseIDToShare = enterpriseID
                            self.checkPostPrivacy(postID: id)
                            
                        }
                    }
                })
            } else {
                print("require publish_actions permissions")
            }
        }
        //Se já tivermos um post, deletamos ele para publicar o novo, caso contrário o Facebook vai juntar os posts em um álbum, o que não nos permite checar a privacidade de cada foto
        else {
            self.textToShare = text
            self.imageToShare = image
            self.entepriseIDToShare = enterpriseID
            self.deletePost(postID: User.instance.lastPostedID!)
        }
        
    }
    
    
    func getNumberOfFriends() {

        let parameters = ["fields": "summary"]
        FBSDKGraphRequest(graphPath: "me/friends", parameters: parameters, httpMethod: "GET").start { (connection, result, error) in

            let userInformations = User.instance

            if let error = error {
                print("erro: \(error.localizedDescription)")
                self.getFriendsCountDelegate?.didGetFriendsCount(error:true)
                return
            }

            if let result = result as? [String: AnyObject] {

                if let summary = result["summary"] as? NSDictionary {
                    if let total_count = summary["total_count"] as? Int {
                        userInformations.numberOfFriends = total_count
                        self.getFriendsCountDelegate?.didGetFriendsCount(error:false)
                    }
                }
            }
        }
    }
    
    func getUserInformations() {
        let parameters = ["fields": "email, first_name, last_name"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start { (connection, result, error) in
            
            let userInformations = User.instance
            
            
            if let error = error {
                print("erro: \(error.localizedDescription)")
                self.getUserDelegate?.didReceiveUser(error: true)
                return
            }
            
            if let result = result as? [String: AnyObject] {
                
                
                
                if let email = result["email"] as? String {
                    userInformations.email = email
                    
                }
                if let phone = result["phone"] as? String {
                    userInformations.phone = phone
                    
                }
                if let firstName = result["first_name"] as? String {
                    userInformations.firstName = firstName
                }
                
                if let lastName = result["last_name"] as? String {
                    userInformations.lastName = lastName
                }
                
        
                
//                if let friends = result["friends"] as? NSDictionary {
//                    if let summary = friends["summary"] as? NSDictionary {
//                        if let total_count = summary["total_count"] as? Int {
//                            userInformations.numberOfFriends = total_count
//                        }
//                    }
//                }
                self.getUserDelegate?.didReceiveUser(error: false)
            }
        }
    }
    
    
}

extension FacebookService: UserServiceDelegate {
    func didReceiveUser() {
        
    }
    
    func didUploadUser(error: Error?) {
        
        self.shareDelegate?.didShare(error, timeError: false, privacyError: privacyError)
    }
    
    
}

extension FacebookService: UploadJsonDelegate {
    func didUploadJson(_ error: Error?) {
        
        
        let userService = UserService(delegate: self)
        userService.updateUserInFirebase()
    }
    
    
}

extension FacebookService: DeletePost {
    func didDeletePost(error: Bool) {
        if !error {
            self.share(text: self.textToShare!, image: self.imageToShare!, enterpriseID: self.entepriseIDToShare!, points: userReach)
        }
    }
    
    
}
