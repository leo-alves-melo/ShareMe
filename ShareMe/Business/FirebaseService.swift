//
//  FirebaseService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Firebase

protocol UploadJsonDelegate {
    func didUploadJson(_ error: Error?)
}

protocol ActivateFunctionDelegate {
    func didActivateFunction(error: Bool)
}

class FirebaseService {
    let enterpriseReference = Database.database().reference(fromURL:"https://shareme-3082d.firebaseio.com/Enterprise")
    let userReference = Database.database().reference(fromURL:"https://shareme-3082d.firebaseio.com/User")
    let couponReference = Database.database().reference(fromURL:"https://shareme-3082d.firebaseio.com/Coupon")
    let postedReference = Database.database().reference(fromURL:"https://shareme-3082d.firebaseio.com/Posted")
    let postReference = Database.database().reference(fromURL:"https://shareme-3082d.firebaseio.com/Post")
    let urlStorage = "gs://shareme-3082d.appspot.com/"
    var storageReference:StorageReference? = nil
    
    let sendCouponEmailFunction = "https://us-central1-shareme-3082d.cloudfunctions.net/sendCouponEmail"
    
    static let instance = FirebaseService()
    
    var uploadJsonDelegate:UploadJsonDelegate? = nil
    var activateFunctionDelegate:ActivateFunctionDelegate? = nil
    
    var userID:String? = nil
    
    private init() {
        
        let storage = Storage.storage()
        self.storageReference = storage.reference(forURL: self.urlStorage)
        self.userID = Auth.auth().currentUser?.providerID
        
        
    }
    
    func getStorageReference(path:String) -> StorageReference {
        
        return self.storageReference!.child(path)
    }
    
    func couponDidUsed(coupon:Coupon, enterprise:Enterprise, verificationCode:String) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        let couponDate = formatter.date(from: coupon.date!)!
        let newDate = couponDate.addingTimeInterval(TimeInterval(coupon.shelfLife! * 60 * 60))
        
        let json: [String: Any] = ["enterpriseEmail":enterprise.email!, "verificationCode":verificationCode, "userName":User.instance.firstName! + " " + User.instance.lastName!,"couponName": coupon.name!, "couponDetail": coupon.detail!, "couponShelfLife": formatter.string(from: newDate), "enterpriseName": enterprise.name!]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        let url = URL(string: sendCouponEmailFunction)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                self.activateFunctionDelegate!.didActivateFunction(error: true)
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                
                print(responseJSON)
            }
            self.activateFunctionDelegate!.didActivateFunction(error: false)
        }
        
        task.resume()
        
    }
    
    func uploadJson(json: [String: AnyObject], reference:DatabaseReference) {
        reference.setValue(json) { (error, databasereference) in
            if let error = error {
                print("Erro ao dar upload de Json: \(error)")
            }
            self.uploadJsonDelegate?.didUploadJson(error)
        }
    }
    
    func uploadAtomicJson(reference: DatabaseReference, function: @escaping (MutableData)->(TransactionResult), completion: @escaping (Error?, Bool, DataSnapshot?) -> Void) {
        reference.runTransactionBlock(function, andCompletionBlock: completion)
    }
    
}
