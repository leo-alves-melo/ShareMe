//
//  EnterpriseService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol EnterpriseServiceDelegate {
    func didReceiveEnterprises(enterprises:[Enterprise])
    
    func didReceiveEnterpriseImage(image: UIImage)
}

class EnterpriseService {
    private var delegate:EnterpriseServiceDelegate
    private let firebaseService = FirebaseService.instance
    private let imagePath = "Enterprise/"
    
    init(delegate:EnterpriseServiceDelegate) {
        self.delegate = delegate
    }
    
    private func createEnterpriseList(data: [String: AnyObject]?) -> [Enterprise] {
        var list:[Enterprise] = []
        
        if let enterprises = data {
            for enterpriseData in enterprises {
                var enterprise = Enterprise()
                
                if let ID = enterpriseData.value["ID"] as? String {
                    enterprise.ID = ID
                }
                if let adress = enterpriseData.value["adress"] as? String {
                    enterprise.adress = adress
                }
                if let category = enterpriseData.value["category"] as? String {
                    enterprise.category = category
                }
                if let email = enterpriseData.value["email"] as? String {
                    enterprise.email = email
                }
                if let name = enterpriseData.value["name"] as? String {
                    enterprise.name = name
                }
                if let city = enterpriseData.value["city"] as? String {
                    enterprise.city = city
                }
                if let state = enterpriseData.value["state"] as? String {
                    enterprise.state = state
                }
                if let phone = enterpriseData.value["phone"] as? String {
                    enterprise.phone = phone
                }
                if let info = enterpriseData.value["info"] as? String {
                    enterprise.info = info
                }
                if let post = enterpriseData.value["post"] as? String {
                    enterprise.post = Post()
                    enterprise.post!.ID = post
                }
                if let coupons = enterpriseData.value["coupon"] as? [String] {
                    
                    var couponsList:[Coupon] = []
                    
                    for couponID in coupons {
                        var coupon = Coupon()
                        coupon.ID = couponID
                        couponsList.append(coupon)
                    }
                    enterprise.coupon = couponsList
                }
                if let imageLink = enterpriseData.value["image"] as? String {
                    enterprise.imageLinked = ImageLinked()
                    enterprise.imageLinked?.link = imageLink
                }
                
                list.append(enterprise)
            }
        }
        
        return list
    }
    
    func retrieveEnterprises(category:String) {
        var enterprisesList:[Enterprise] = []
        
        self.firebaseService.enterpriseReference.queryOrdered(byChild: "category").queryEqual(toValue: category).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            enterprisesList = self.createEnterpriseList(data: data)
            
            self.delegate.didReceiveEnterprises(enterprises: enterprisesList)
        }
    }
    
    func retrieveEnterprise(ID:String) {
        var enterprisesList:[Enterprise] = []
        
        self.firebaseService.enterpriseReference.queryOrdered(byChild: "ID").queryEqual(toValue: ID).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            enterprisesList = self.createEnterpriseList(data: data)
            
            self.delegate.didReceiveEnterprises(enterprises: enterprisesList)
        }
    }
    
    func retrieveAllEnterprises() {
        var enterprisesList:[Enterprise] = []
        
        self.firebaseService.enterpriseReference.queryOrdered(byChild: "name").observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            enterprisesList = self.createEnterpriseList(data: data)
            
            self.delegate.didReceiveEnterprises(enterprises: enterprisesList)
        }
    }
    
    func retrieveEnterpriseImage(enterprise:Enterprise) {
        let imageDownloader = ImageDownloader(delegate: self)
        imageDownloader.getImageFromDatabase(path: imagePath + enterprise.imageLinked!.link!)
    }
}

extension EnterpriseService: ImageDownloaderDelegate {
    func didReceiveImage(image: UIImage) {
        self.delegate.didReceiveEnterpriseImage(image: image)
    }
    
    
}
