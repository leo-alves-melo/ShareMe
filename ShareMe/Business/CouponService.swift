//
//  CouponService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Firebase

protocol CouponServiceDelegate {
    func didReceiveCoupon(coupons: [Coupon])
    func didAddCouponToUser(error:Bool, untransferible: Bool, noMoreCoupon: Bool)
}

class CouponService {
    private var delegate:CouponServiceDelegate
    private let firebaseService = FirebaseService.instance
    private let imagePath = "Coupon/"
    
    init(delegate:CouponServiceDelegate) {
        self.delegate = delegate
    }
    
    private func createCoupon(data: [String: AnyObject]?) -> [Coupon] {
        
        var couponList:[Coupon] = []
        
        if let couponDatas = data {
            
            for couponData in couponDatas {
                var coupon = Coupon()
                
                if let ID = couponData.value["ID"] as? String {
                    coupon.ID = ID
                }
                if let enterprise = couponData.value["enterprise"] as? String {
                    coupon.enterprise = enterprise
                }
                if let name = couponData.value["name"] as? String {
                    coupon.name = name
                }
                if let detail = couponData.value["detail"] as? String {
                    coupon.detail = detail
                }
                if let price = couponData.value["price"] as? Int {
                    coupon.price = price
                }
                if let quantity = couponData.value["quantity"] as? Int {
                    coupon.quantity = quantity
                }
                if let untransferible = couponData.value["untransferible"] as? Bool {
                    coupon.untransferible = untransferible
                }
                if let shelfLife = couponData.value["shelfLife"] as? Int {
                    coupon.shelfLife = shelfLife
                }
                if let date = couponData.value["date"] as? String {
                    coupon.date = date
                }
                if let imageLink = couponData.value["image"] as? String {
                    coupon.imageLinked = ImageLinked()
                    coupon.imageLinked?.link = self.imagePath + imageLink
                }
                
                
                
                couponList.append(coupon)
            }
        }
        
        return couponList
        
    }
    
    func retrieveCoupon(ID:String) {
        
        var couponList:[Coupon] = []
        
        self.firebaseService.couponReference.queryOrdered(byChild: "ID").queryEqual(toValue: ID).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            couponList = self.createCoupon(data: data)
            
            self.delegate.didReceiveCoupon(coupons: couponList)
        }
    }
    
    func retireveCoupon(enterprise:Enterprise) {
        
        var couponList:[Coupon] = []
        
        self.firebaseService.couponReference.queryOrdered(byChild: "enterprise").queryEqual(toValue: enterprise.ID!).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            couponList = self.createCoupon(data: data)
            
            self.delegate.didReceiveCoupon(coupons: couponList)
        }
    }
    
    func checkCouponShefLife(coupon: Coupon, userCoupon: Coupon) -> Bool {

        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm"
        let couponDate = formatter.date(from: userCoupon.date!)!
        
        let currentDate = Date()
        
        if currentDate > couponDate.addingTimeInterval(TimeInterval(coupon.shelfLife! * 60 * 60)) {
            //Cupom venceu
            return false
        }
        else {
            //Cupom nao venceu
            return true
        }

        
    }
    
    func addCouponToUser(coupon: Coupon) {
        
        if let _ = User.instance.coupon {
            
            //Checa se já temos o cupom, e se tivermos, checa se ele já venceu, e se não, vê se podemos mesmo ter dois do mesmo
            for var couponIndex in 0..<User.instance.coupon!.count {
                if coupon.ID! == User.instance.coupon![couponIndex].ID! {
                    if coupon.untransferible! {
                        
                        if checkCouponShefLife(coupon: coupon, userCoupon: User.instance.coupon![couponIndex]) {
                            //Error: você já tem esse cupon
                            self.delegate.didAddCouponToUser(error: true, untransferible: true, noMoreCoupon: false)
                            return
                        }
                        
                    }
                    
                }
                
            }
            
            self.firebaseService.uploadAtomicJson(reference: firebaseService.couponReference.child(coupon.ID!).child("quantity"), function: { (currentData) -> (TransactionResult) in
                if let quantity = currentData.value as? Int {
                    if quantity > 0 {
                        currentData.value = quantity - 1
                        
                        return TransactionResult.success(withValue: currentData)
                    }
                        //Não tem mais cupons
                    else {
                        return TransactionResult.abort()
                    }
                }
                return TransactionResult.success(withValue: currentData)
            }, completion: { (error, bool, snapshot) in
                
                if bool {
                    var newCoupon = Coupon()
                    newCoupon.ID = coupon.ID!
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy HH:mm"
                    newCoupon.date = formatter.string(from: Date())
                    User.instance.coupon!.append(newCoupon)
                    //Subtrai os pontos do usuário
                    
                    for coinIndex in 0..<User.instance.coin!.count {
                        
                        if User.instance.coin![coinIndex].ID! == coupon.enterprise! {
                            User.instance.coin![coinIndex].quantity = User.instance.coin![coinIndex].quantity! - coupon.price!
                        }
                        
                    }
                    
                    let userService = UserService(delegate: self)
                    userService.updateUserInFirebase()
                }
                else {
                    self.delegate.didAddCouponToUser(error: true, untransferible: false, noMoreCoupon: true)
                }
                
                
            })
            
            
        }
            //Se ainda não houver cupons
        else {
            
            self.firebaseService.uploadAtomicJson(reference: firebaseService.couponReference.child(coupon.ID!).child("quantity"), function: { (currentData) -> (TransactionResult) in
                if let quantity = currentData.value as? Int {
                    if quantity > 0 {
                        currentData.value = quantity - 1
                        
                        return TransactionResult.success(withValue: currentData)
                    }
                        //Não tem mais cupons
                    else {
                        return TransactionResult.abort()
                    }
                }
                return TransactionResult.success(withValue: currentData)
            }, completion: { (error, bool, snapshot) in
                
                if bool {
                    var newCoupon = Coupon()
                    newCoupon.ID = coupon.ID!
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy HH:mm"
                    newCoupon.date = formatter.string(from: Date())
                    User.instance.coupon = [newCoupon]
                    //Subtrai os pontos do usuário
                    
                    for coinIndex in 0..<User.instance.coin!.count {
                        
                        if User.instance.coin![coinIndex].ID! == coupon.enterprise! {
                            User.instance.coin![coinIndex].quantity = User.instance.coin![coinIndex].quantity! - coupon.price!
                        }
                        
                    }
                    
                    let userService = UserService(delegate: self)
                    userService.updateUserInFirebase()
                }
                else {
                    self.delegate.didAddCouponToUser(error: true, untransferible: false, noMoreCoupon: true)
                }
                
                
            })
            
        }
        
    }
}

extension CouponService: UserServiceDelegate {
    func didReceiveUser() {
        
    }
    
    func didUploadUser(error: Error?) {
        if let _ = error {
            self.delegate.didAddCouponToUser(error: true, untransferible: false, noMoreCoupon: false)
        }
        else {
            self.delegate.didAddCouponToUser(error: false, untransferible: false, noMoreCoupon: false)
        }
    }
    
    
}

