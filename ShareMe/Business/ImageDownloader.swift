//
//  ImageDownloader.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation
import Firebase

protocol ImageDownloaderDelegate {
    func didReceiveImage(image: UIImage)
}

class ImageDownloader {
    
    let firebaseService = FirebaseService.instance
    var imageDownloaderDelegate:ImageDownloaderDelegate
    
    init(delegate:ImageDownloaderDelegate) {
        self.imageDownloaderDelegate = delegate
    }
    
    func getImageFromDatabase(path:String?) {
        
        if let path = path {
            let imageRef = firebaseService.getStorageReference(path: path)
            imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    
                    print(error.localizedDescription)
                    
                } else {
                    self.imageDownloaderDelegate.didReceiveImage(image: UIImage(data: data!)!)
                }
            }
        }
    }
}
