//
//  PostService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 16/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol PostServiceDelegate {
    func didReceivePost(post: Post)
    func didReceivePostImage(image: UIImage)
}

class PostService {
    private var delegate:PostServiceDelegate
    private let firebaseService = FirebaseService.instance
    private let imagePath = "Post/"
    
    init(delegate:PostServiceDelegate) {
        self.delegate = delegate
    }
    
    private func createPost(data: [String: AnyObject]?) -> Post {
        let post:Post = Post()
        
        if let postData = data?.first {
            
           
            if let ID = postData.value["ID"] as? String {
                post.ID = ID
            }
            if let text = postData.value["text"] as? String {
                post.text = text
            }
            if let imageLink = postData.value["image"] as? String {
                post.imageLinked = ImageLinked()
                post.imageLinked!.link = imageLink
            }
            if let enterprise = postData.value["enterprise"] as? String {
                post.enterprise = Enterprise()
                post.enterprise!.ID = enterprise
            }
            
        }
        
        return post
    }
    
    func retrievePost(enterpriseID:String) {
        self.firebaseService.postReference.queryOrdered(byChild: "enterprise").queryEqual(toValue: enterpriseID).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            let post = self.createPost(data: data)
            
            self.delegate.didReceivePost(post: post)
        }
    }
    
    func retrievePostImage(path: String) {
        let imageDownloader = ImageDownloader(delegate: self)
        imageDownloader.getImageFromDatabase(path: imagePath + path)
    }
}

extension PostService: ImageDownloaderDelegate {
    func didReceiveImage(image: UIImage) {
        self.delegate.didReceivePostImage(image: image)
    }
    
    
}
