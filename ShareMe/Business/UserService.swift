//
//  UserService.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol UserServiceDelegate {
    func didReceiveUser()
    func didUploadUser(error:Error?)
}

class UserService {
    private var delegate:UserServiceDelegate? = nil
    private let firebaseService = FirebaseService.instance
    private let imagePath = "User/"
    
    init(delegate:UserServiceDelegate) {
        self.delegate = delegate
    }
    
    init() {
        
    }
    
    private func createUser(data: [String: AnyObject]?) {
        
        if let userDatas = data {
            
            for userData in userDatas {
                let user = User.instance
                
                if let ID = userData.value["ID"] as? String {
                    user.ID = ID
                }
                if let firstName = userData.value["firstName"] as? String {
                    user.firstName = firstName
                }
                if let lastName = userData.value["lastName"] as? String {
                    user.lastName = lastName
                }
                if let email = userData.value["email"] as? String {
                    user.email = email
                }
                if let phone = userData.value["phone"] as? String {
                    user.phone = phone
                }
                if let friends = userData.value["friends"] as? Int {
                    user.numberOfFriends = friends
                }
                if let lastPostedID = userData.value["lastPostedID"] as? String {
                    user.lastPostedID = lastPostedID
                }
                if let lastPosted = userData.value["lastPosted"] as? String {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy HH:mm"
                    user.lastPosted = formatter.date(from: lastPosted)!
                }
                if let coupon = userData.value["coupon"] as? [Dictionary<String, Any>] {
                    user.coupon = [Coupon]()
                    
                    for couponData in coupon {
                        var newCoupon = Coupon()
                        newCoupon.ID = couponData["ID"] as? String
                        newCoupon.date = couponData["date"] as? String
                        user.coupon?.append(newCoupon)
                    }
                    
                    
                }
                
                if let coin = userData.value["coin"] as? [Dictionary<String, Any>] {
                    user.coin = [Coin]()
                    for coinData in coin {
                        var newCoin = Coin()
                        newCoin.ID = coinData["ID"] as? String
                        newCoin.quantity = coinData["quantity"] as? Int
                        user.coin?.append(newCoin)
                    }
                }
                
            }
        }
            
            

    }
    
    func retrieveUser(ID:String) {
         self.firebaseService.userReference.queryOrdered(byChild: "ID").queryEqual(toValue: ID).observeSingleEvent(of: .value) { (snapshot) in
            
            let data = snapshot.value as? [String: AnyObject]
            self.createUser(data: data)
            
            
            if let delegate = self.delegate {
                delegate.didReceiveUser()
            }
            
        }
    }
    
    func updateUserInFirebase() {
        
        let user = User.instance
        
        let json: NSMutableDictionary = NSMutableDictionary()
        json.setValue(user.ID!, forKey: "ID")
        
        if let email = user.email {
            json.setValue(email, forKey: "email")
        }
        if let firstName = user.firstName {
            json.setValue(firstName, forKey: "firstName")
        }
        if let lastName = user.lastName {
            json.setValue(lastName, forKey: "lastName")
        }
        if let phone = user.phone {
            json.setValue(phone, forKey: "phone")
        }
        if let lastPostedID = user.lastPostedID {
            json.setValue(lastPostedID, forKey: "lastPostedID")
        }
        if let numberOfFriends = user.numberOfFriends {
            json.setValue(numberOfFriends, forKey: "friends")
        }
        if let lastPosted = user.lastPosted {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            json.setValue(formatter.string(from: lastPosted), forKey: "lastPosted")
        }
        
        if user.coupon != nil {
            let jsonArrayCoupon: NSMutableArray = NSMutableArray()
            for coupon in user.coupon! {
                let jsonCoupon = NSMutableDictionary()
                jsonCoupon.setValue(coupon.ID!, forKey: "ID")
                jsonCoupon.setValue(coupon.date!, forKey: "date")
                
                jsonArrayCoupon.add(jsonCoupon)
            }
            
            json.setValue(jsonArrayCoupon, forKey: "coupon")
            
        }
        
        if user.coin != nil {
            let jsonArrayCoin: NSMutableArray = NSMutableArray()
            for coin in user.coin! {
                let jsonCoin = NSMutableDictionary()
                jsonCoin.setValue(coin.ID!, forKey: "ID")
                jsonCoin.setValue(coin.quantity!, forKey: "quantity")
                
                jsonArrayCoin.add(jsonCoin)
            }
            
            json.setValue(jsonArrayCoin, forKey: "coin")
        }
        
        
        
        FirebaseService.instance.uploadJsonDelegate = self
        FirebaseService.instance.uploadJson(json: json as! [String : AnyObject], reference: FirebaseService.instance.userReference.child(user.ID!))
        
    }
    
    func deleteUser() {
        User.instance.firstName = nil
        User.instance.coin = nil
        User.instance.coupon = nil
        User.instance.email = nil
        User.instance.ID = nil
        User.instance.lastName = nil
        User.instance.lastPosted = nil
        User.instance.lastPostedID = nil
        User.instance.numberOfFriends = nil
        User.instance.phone = nil
    }
}

extension UserService: UploadJsonDelegate {
    func didUploadJson(_ error: Error?) {
        self.delegate?.didUploadUser(error: error)
    }
    
    
}
