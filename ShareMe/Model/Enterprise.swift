//
//  Enterprise.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Enterprise {
    var name:String? = nil
    var ID:String? = nil
    var adress:String? = nil
    var email:String? = nil
    var imageLinked:ImageLinked? = nil
    var phone:String? = nil
    var coupon:[Coupon]? = nil
    var category:String? = nil
    var city:String? = nil
    var state:String? = nil
    var post:Post? = nil
    var info:String? = nil
    
    func present() {
        print("----------ENTERPRISE------------")
        print("Meu nome é \(self.name!)")
        print("Meu email é \(self.email!)")
        print("Endereço: \(self.adress!)")
        print("ID: \(self.ID!)")
        print("phone: \(self.phone!)")
        print("Category: \(self.category!)")
        print("cupons:")
        for coupon in self.coupon! {
            print("- \(coupon.ID!)")
        }
        print("image: \(self.imageLinked!.link!)")
        print("----------------------")
    }
}
