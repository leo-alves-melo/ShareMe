//
//  ImageDescribed.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct ImageLinked {
    var link:String? = nil
    var image:UIImage? = nil
}
