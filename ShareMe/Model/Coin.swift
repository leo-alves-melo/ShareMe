//
//  Coin.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Coin {
    var ID:String? = nil
    var quantity:Int? = nil
}
