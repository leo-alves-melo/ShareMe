//
//  UserInformations.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 08/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

protocol HaveUserInformations {
    var userInformations:User? {get set}
}

class User {
    var numberOfFriends: Int? = nil
    var firstName: String? = nil
    var lastName: String? = nil
    var email:String? = nil
    var coin:[Coin]? = nil
    var coupon:[Coupon]? = nil
    var ID:String? = nil
    var phone:String? = nil
    var lastPosted:Date? = nil
    var lastPostedID:String? = nil
    
    
    static let instance = User()
    
    private init() {
        
    }
    
    func present() {
        print("------------USER----------")
        print("Meu nome é \(self.firstName!), com sobrenome \(self.lastName!)")
        print("Meu email é \(self.email!)")
        //print("Eu tenho \(self.numberOfFriends!) amigos")
        print("pontos")
        for coin in self.coin! {
            print("- \(coin.ID!): \(coin.quantity!)")
        }
        print("cupons")
        for coupon in self.coupon! {
            print("- \(coupon.ID!): \(coupon.quantity!)")
        }
        print("----------------------")
    }
}
