//
//  Coupon.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 09/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

struct Coupon {
    var detail:String? = nil
    var imageLinked:ImageLinked? = nil
    var name:String? = nil
    var quantity:Int? = nil
    var untransferible:Bool? = nil
    var ID:String? = nil
    var price:Int? = nil
    var enterprise:String? = nil
    var shelfLife:Int? = nil
    var date:String? = nil
}
