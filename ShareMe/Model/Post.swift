//
//  Post.swift
//  ShareMe
//
//  Created by Leonardo Alves de Melo on 16/01/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class Post {
    var ID:String? = nil
    var imageLinked:ImageLinked? = nil
    var text:String? = nil
    var enterprise:Enterprise? = nil
}
