const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
var database = admin.database();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// 43cf3d2fdd4603d3c6bf1a002c613ec3b9d4d32d

const nodemailer = require('nodemailer');
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});
const APP_NAME = 'Desconta aê!';

const rp = require('request-promise');
const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
const secureCompare = require('secure-compare');
// Maximum concurrent account deletions.
const MAX_CONCURRENT = 3;



//FACEBOOK
var graph = require('fbgraph');
graph.setAccessToken('EAAH1wwU6MRYBAHpKg7V4htBqlwmI67iRji4xqvWpI030jUP720C9AUEQQsUHl324iCvItpxnIHwRZCqZCqUBhQHVeIJtpFWl1JArsM2RWulLVyaTeJJ6G2oL8RhByekgKCXrzvABOzNJc5dvZCVgtUdW6tCT7pYmSWzq7BODXiqZCttJNpr6FZAHKruZBbCr1ruPtv21rZBZATbPNeVZAtSY76SRUmfsDJRSfxooVgsC1pQZDZD');




exports.sendCouponEmail = functions.https.onRequest((req, res) => {
// [END onCreateTrigger]
  // [START eventAttributes]
  var verificationCode = req.body.verificationCode;
  var enterpriseEmail = req.body.enterpriseEmail;
  var couponName = req.body.couponName;
  var userName = req.body.userName;
  var couponDetail = req.body.couponDetail;
  var couponShelfLife = req.body.couponShelfLife;
  var enterpriseName = req.body.enterpriseName;

  sendEmail(enterpriseEmail, verificationCode, userName, couponName, couponDetail, couponShelfLife, enterpriseName);
  res.status(403).send("ok: " + enterpriseName);
});





/**
* When requested this Function will delete every user accounts that has been inactive for 30 days.
* The request needs to be authorized by passing a 'key' query parameter in the URL. This key must
* match a key set as an environment variable using `firebase functions:config:set cron.key="YOUR_KEY"`.
*/
exports.updateCoins = functions.https.onRequest((req, res) => {
	const key = req.query.key;

	// Exit if the keys don't match
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key,
		'matches the cron.key attribute in `firebase env:get`');
		
		//return;
	}


	readPosted(res);

	

});

function updateJson(reference, json) {
	
	database.ref(reference).update(json);
}

function readPosted(res) {

	return database.ref().child('Posted').once('value').then(function(snapshot) {
		
		var json = snapshot.val()

		var keys = Object.keys(json);
		for(var i=0;i<keys.length;i++){
			var posted = keys[i];

			var postID = posted['postID'];
			var params = { fields: "total_count", summary:true };
			
			graph.get(posted + '/likes', params, function(err, res2) {
  				res.status(403).send('Meu post2: ' + posted + ', Meus dados: ' + res2 + ', meu erro: ' + err.toString());
			});
			
		}
	});
}

// Sends a welcome email to the given user.
function sendEmail(email, verificationCode, userName, couponName, couponDetail, couponShelfLife, enterpriseName) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@firebase.com>`,
    to: email
  };

  // The user subscribed to the newsletter.
  mailOptions.subject = `[Desconta aê!] ${userName} utilizou seu cupom!`;
  mailOptions.text = `Olá ${enterpriseName},\n\nO seu cupom \"${couponName}\", de acabou de ser utilizado por ${userName}, que adquiriu o benefício de \"${couponDetail}\". Ele é válido até ${couponShelfLife}. Confira se o cupom que será apresentado a você contém o código de verificação \"${verificationCode}\", se não, não o aceite.\n\nAtenciosamente,\nEquipe Desconta aê!`;
  return mailTransport.sendMail(mailOptions).then(() => {
    console.log('New email sent to:', email);
  });
}
